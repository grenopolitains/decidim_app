# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# You can remove the 'faker' gem if you don't want Decidim seeds.
# Decidim.seed!

unless Rails.env.development?
  print "Seeds are generated in development env only"
  return
end

print "Creating seeds for decidim-system...\n"

Decidim::System::Admin.find_or_initialize_by(email: "system@example.org").update!(
  password: "decidim123456",
  password_confirmation: "decidim123456"
)

print "Creating seeds for decidim-core...\n"

organization = Decidim::Organization.first || Decidim::Organization.create!(
  name: "Grenopolitains",
  host: "localhost",
  description: "La plate-forme de prise de décisions pour tous les Grenopolitains",
  default_locale: Decidim.default_locale,
  available_locales: Decidim.available_locales,
  reference_prefix: :grenopolitains,
  available_authorizations: Decidim.authorization_workflows.map(&:name),
  users_registration_mode: :enabled,
  tos_version: Time.current,
  badges_enabled: true,
  send_welcome_notification: true
)

admin = Decidim::User.find_or_initialize_by(email: "admin@example.org")

admin.update!(
  name: "John Admin",
  nickname: "john_admin",
  password: "decidim123456",
  password_confirmation: "decidim123456",
  organization: organization,
  confirmed_at: Time.current,
  locale: I18n.default_locale,
  admin: true,
  tos_agreement: true,
  personal_url: Faker::Internet.url,
  about: Faker::Lorem.paragraph(2),
  accepted_tos_version: organization.tos_version
)

regular_user = Decidim::User.find_or_initialize_by(email: "user@example.org")

regular_user.update!(
  name: "John User",
  nickname: "john_user",
  password: "decidim123456",
  password_confirmation: "decidim123456",
  confirmed_at: Time.current,
  locale: I18n.default_locale,
  organization: organization,
  tos_agreement: true,
  personal_url: Faker::Internet.url,
  about: Faker::Lorem.paragraph(2),
  accepted_tos_version: organization.tos_version
)
