# Grenopolitains (plateforme Decidim)

Plateforme participative de participation démocratique pour les citoyens.

Ce dépôt contient le code de la plateforme Decidim destinée aux Grenopolitains
et Grenopolitaines. Le code est basé sur [Decidim](https://github.com/decidim/decidim).

## Contexte

Decidim est une application Ruby on Rails (RoR) et utilise une base de données
PostgreSQL. Si vous souhaitez contribuer, nous vous recommandons au moins de
commencer par suivre [le tutoriel de RoR](https://guides.rubyonrails.org/getting_started.html).

Par soucis de simplicité, et comme la plateforme est à destination d'une
population en grande majorité francophone, la documentation est rédigée
elle-même en français. Manipulant néanmoins du code et des concepts faisant
grandement appels à la langue anglaise, des anglicismes risquent fortement de
s'y glisser. Certaines documentations auxquelles nous faisons également
référence sont aussi, par conséquent, en langue anglaise.

## Configuration de l'environnement de développement

Nous recommandons d'utiliser Docker puisque c'est ce que nous utilisons
nous-mêmes. Pour l'installer, nous vous suggérons de suivre [la documentation
officielle](https://docs.docker.com/install/). Vous devrez aussi installer
[docker-compose](https://docs.docker.com/compose/install/).

Ensuite, il vous faut installer les dépendances :

```console
$ d/bundle install
```

Attention, notez bien que la commande commence par `d/`. Nous faisons en
réalité appel à un script qui lance `docker-compose`. L'installation des
dépendances se fait donc à l'intérieur d'un conteneur Docker. Le répertoire où
elles sont installées est un volume Docker partagé afin qu'elles soient
persistantes à travers plusieurs appels à `docker-compose`. Pour plus
de détails vous pouvez jeter un coup d'œil au fichier `d/bundle` concerné
ainsi que le fichier `docker-compose.yml`. Toutes les commandes Ruby et RoR
habituelles doivent être exécutées de la même manière.

La commande précédente commence tout d'abord par télécharger l'image Docker
`decidim/decidim:0.16.0-dev`. Malheureusement cette image est particulièrement
grosse et peut prendre du temps à télécharger en fonction de votre connexion.

Maintenant que les dépendances sont installées, il est nécessaire d'initialiser
les données de la base de données :

```console
$ d/rails db:setup
```

La base est initialisée à partir des informations contenues dans le fichier
`db/schema.rb` ainsi que les "seeds" du fichier `db/seeds.rb`.

Enfin, lancez le serveur avec la commande suivante :

```console
$ docker-compose up
```

Cela devrait lancer le serveur [en local sur le port 3000](http://localhost:3000).

Trois utilisateurs sont créés par défaut :

- system@example.org (`decidim123456`)
- admin@example.org (`decidim123456`)
- user@example.org (`decidim123456`)

## Configuration de l'environnement de production

Aujourd'hui nous disposons d'un dépot spécifique pour la mise en production de
Decidim. Ce dépôt n'a pas pour but de devenir public car il pourrait donner des
informations critiques sur le serveur.

Nous envisageons toutefois à terme de documenter plus en détails cette partie.
Nous remettons cette tâche à plus tard principalement par manque de temps à
accorder au sujet, mais n'hésitez pas à nous faire part de votre attente dans
[les tickets du dépôt](https://framagit.org/grenopolitains/decidim_app/issues).
